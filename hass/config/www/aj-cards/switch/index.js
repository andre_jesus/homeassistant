import {
  LitElement,
  html
} from "https://unpkg.com/lit-element@2.0.1/lit-element.js?module";

class AjSwitch extends LitElement {
  static get properties() {
    return {
      hass: {},
      config: {}
    };
  }

  	render() {
    	return html`
      		<link rel="stylesheet" href="/local/aj-cards/switch/index.css">
			<h1>${this.config.title}</h1>
			<div class="aj-switch-wrapper">
				${this.config.entities.map(entity => {
					const entityState = this.hass.states[entity];
					return entityState ?
					html`
						<div
							class="aj-switch ${entityState.state === "on" ? "aj-switch--on" : ''}"
							@click="${ev => this._toggle(entityState)}"
							>
							<div class="aj-switch__label">${entityState.attributes.friendly_name}</div>
							<div class="aj-switch__info">
								<ul>
									<li>Consumo: <span>120W</span></li>
								</ul>
							</div>
							<div class="aj-switch__state">${entityState.state}</div>
							<div class="aj-switch__background-image"></div>
						</div>`
					: html`
						<div class="aj-switch">
							<div class="aj-switch__label">Entity not found!</div>
						</div>`;
				})}
			</div>
    	`;
  	}

	setConfig(config) {
		if (!config.entities) {
		throw new Error("You need to define entities");
		}
		this.config = config;
	}

	_toggle(state) {
		console.log('toggle');
		this.hass.callService("homeassistant", "toggle", {
		  	entity_id: state.entity_id
		});
	}

}
customElements.define("aj-switch", AjSwitch);