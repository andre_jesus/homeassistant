let root = document.documentElement;
/**
 * variables
 */
let baseColor = '#2192FC';
root.style.setProperty('--divider-color', "none");
root.style.setProperty('--app-header-background-color', "transparent");
root.style.setProperty('--app-header-text-color', baseColor);
root.style.setProperty('--paper-tabs-selection-bar-color', baseColor);

var homeAssistant = document.getElementsByTagName('home-assistant')[0];
var homeAssistantMain = homeAssistant.shadowRoot.querySelector("home-assistant-main");
var appDrawerLayout = homeAssistantMain.shadowRoot.querySelector("app-drawer-layout");
var appDrawer = appDrawerLayout.querySelector("app-drawer");
var partialPanelResolver = appDrawerLayout.querySelector("partial-panel-resolver");
var haPanelLovelace = partialPanelResolver.querySelector("ha-panel-lovelace");
var huiRoot = haPanelLovelace.shadowRoot.querySelector("hui-root");
var haAppLayout = huiRoot.shadowRoot.querySelector("ha-app-layout");
var appHeader = haAppLayout.querySelector("app-header");
var appToolbar = appHeader.querySelector("app-toolbar");
console.log(appToolbar);

/**
 * header
 */
appToolbar.style.backgroundColor = 'transparent';

/**
 * Side bar
 */
var sideBar = appDrawer.querySelector("ha-sidebar");
var sideBarMenu = sideBar.shadowRoot.querySelector(".menu");
var sideBarMenuIcon = sideBar.shadowRoot.querySelector("mwc-icon-button");
sideBarMenuIcon.style.color = baseColor;

var sideBarItems = sideBar.shadowRoot.querySelectorAll('paper-listbox a');
sideBarItems.forEach(menuItem => {
    menuItem.querySelector('paper-icon-item').style.height = '56px';
    menuItem.querySelector('paper-icon-item').style.paddingLeft = '8px';
    menuItem.querySelector('paper-icon-item .item-text').style.fontSize = '1.1em';
    menuItem.querySelector('paper-icon-item .item-text').style.color = 'white';
    menuItem.querySelector('paper-icon-item ha-svg-icon, paper-icon-item ha-icon').style.color = 'white';
});

var sideBarNotificationItem = sideBar.shadowRoot.querySelector('.notifications-container .notifications');
sideBarNotificationItem.style.height = '56px';
sideBarNotificationItem.style.paddingLeft = '8px';
sideBarNotificationItem.querySelector('.item-text').style.fontSize = '1.1em';
sideBarNotificationItem.querySelector('.item-text').style.color = 'white';
sideBarNotificationItem.querySelector('paper-icon-item ha-svg-icon, paper-icon-item ha-icon').style.color = 'white';


console.log('Casa Theme | Ui Styles Updated');


/*sideBarMenu.style.backgroundColor = "transparent";
sideBarMenu.style.color = "white";
sideBarMenu.querySelector('.title').innerHTML = "Casa";
*/

